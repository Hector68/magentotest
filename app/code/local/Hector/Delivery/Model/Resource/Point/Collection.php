<?php
/**
 * Date: 05.03.2017
 * Time: 13:05
 */ 
class Hector_Delivery_Model_Resource_Point_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('hector_delivery/point');
    }

}