<?php


class Hector_ProductObserver_Model_Observer
{
    public function catalog_product_load_after(Varien_Event_Observer  $observer)
    {

        /**
         * @var $product Mage_Catalog_Model_Product
         */
        $product = $observer->getProduct();
        $product->setFinalPrice($product->getPrice() + 10);
    }
    
}