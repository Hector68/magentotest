<?php
$installer = $this;
$installer->startSetup();
$tableName = $installer->getTable('ecomgems_salesforecast/forecast');
// Check if the table already exists
$installer->getConnection()->dropColumn($tableName, 'status');
$installer->endSetup();