<?php
$installer = $this;
$installer->startSetup();
$tableName = $installer->getTable('ecomgems_salesforecast/forecast');
// Check if the table already exists
if ($installer->getConnection()->isTableExists($tableName)) {
    $table = $installer->getConnection();

    $table->addIndex(
        $tableName,
        $installer->getIdxName(
            'ecomgems_salesforecast/forecast',
            array(
                'customer_id',
                'year',
                'month',
            ),
            Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
        ),
        array(
            'customer_id',
            'year',
            'month',
        ),
        Varien_Db_Adapter_Interface::INDEX_TYPE_UNIQUE
    );
}

$installer->endSetup();