<?php

/**
 * Sales Forecast admin controller
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast

 */
class EcomGems_SalesForecast_Adminhtml_Salesforecast_ForecastController extends EcomGems_SalesForecast_Controller_Adminhtml_SalesForecast
{
    /**
     * init the sales forecast
     *
     * @access protected
     * @return EcomGems_SalesForecast_Model_Forecast
     */
    protected function _initForecast()
    {
        $forecastId  = (int) $this->getRequest()->getParam('id');
        $forecast    = Mage::getModel('ecomgems_salesforecast/forecast');
        if ($forecastId) {
            $forecast->load($forecastId);
        }
        Mage::register('current_forecast', $forecast);
        return $forecast;
    }

    /**
     * default action
     *
     * @access public
     * @return void
     * 
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title(Mage::helper('ecomgems_salesforecast')->__('Forecast'))
             ->_title(Mage::helper('ecomgems_salesforecast')->__('Sales Forecasts'));
        $this->renderLayout();
    }

    /**
     * grid action
     *
     * @access public
     * @return void
     * 
     */
    public function gridAction()
    {
        $this->loadLayout()->renderLayout();
    }

    /**
     * edit sales forecast - action
     *
     * @access public
     * @return void
     * 
     */
    public function editAction()
    {
        $forecastId    = $this->getRequest()->getParam('id');
        $forecast      = $this->_initForecast();
        if ($forecastId && !$forecast->getId()) {
            $this->_getSession()->addError(
                Mage::helper('ecomgems_salesforecast')->__('This sales forecast no longer exists.')
            );
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getForecastData(true);
        if (!empty($data)) {
            $forecast->setData($data);
        }
        Mage::register('forecast_data', $forecast);
        $this->loadLayout();
        $this->_title(Mage::helper('ecomgems_salesforecast')->__('Forecast'))
             ->_title(Mage::helper('ecomgems_salesforecast')->__('Sales Forecasts'));
        if ($forecast->getId()) {
            $this->_title($forecast->getForecastAmount());
        } else {
            $this->_title(Mage::helper('ecomgems_salesforecast')->__('Add sales forecast'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    /**
     * new sales forecast action
     *
     * @access public
     * @return void
     * 
     */
    public function newAction()
    {
        $this->_forward('edit');
    }

    /**
     * save sales forecast - action
     *
     * @access public
     * @return void
     * 
     */
    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost('forecast')) {
            try {
                $forecast = $this->_initForecast();
                $forecast->addData($data);
                $forecast->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ecomgems_salesforecast')->__('Sales Forecast was successfully saved')
                );
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $forecast->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setForecastData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ecomgems_salesforecast')->__('There was a problem saving the sales forecast.')
                );
                Mage::getSingleton('adminhtml/session')->setForecastData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ecomgems_salesforecast')->__('Unable to find sales forecast to save.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * delete sales forecast - action
     *
     * @access public
     * @return void
     * 
     */
    public function deleteAction()
    {
        if ( $this->getRequest()->getParam('id') > 0) {
            try {
                $forecast = Mage::getModel('ecomgems_salesforecast/forecast');
                $forecast->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ecomgems_salesforecast')->__('Sales Forecast was successfully deleted.')
                );
                $this->_redirect('*/*/');
                return;
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ecomgems_salesforecast')->__('There was an error deleting sales forecast.')
                );
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                Mage::logException($e);
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(
            Mage::helper('ecomgems_salesforecast')->__('Could not find sales forecast to delete.')
        );
        $this->_redirect('*/*/');
    }

    /**
     * mass delete sales forecast - action
     *
     * @access public
     * @return void
     * 
     */
    public function massDeleteAction()
    {
        $forecastIds = $this->getRequest()->getParam('forecast');
        if (!is_array($forecastIds)) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('ecomgems_salesforecast')->__('Please select sales forecasts to delete.')
            );
        } else {
            try {
                foreach ($forecastIds as $forecastId) {
                    $forecast = Mage::getModel('ecomgems_salesforecast/forecast');
                    $forecast->setId($forecastId)->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('ecomgems_salesforecast')->__('Total of %d sales forecasts were successfully deleted.', count($forecastIds))
                );
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(
                    Mage::helper('ecomgems_salesforecast')->__('There was an error deleting sales forecasts.')
                );
                Mage::logException($e);
            }
        }
        $this->_redirect('*/*/index');
    }

    

    /**
     * export as csv - action
     *
     * @access public
     * @return void
     * 
     */
    public function exportCsvAction()
    {
        $fileName   = 'forecast.csv';
        $content    = $this->getLayout()->createBlock('ecomgems_salesforecast/adminhtml_forecast_grid')
            ->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as MsExcel - action
     *
     * @access public
     * @return void
     * 
     */
    public function exportExcelAction()
    {
        $fileName   = 'forecast.xls';
        $content    = $this->getLayout()->createBlock('ecomgems_salesforecast/adminhtml_forecast_grid')
            ->getExcelFile();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * export as xml - action
     *
     * @access public
     * @return void
     * 
     */
    public function exportXmlAction()
    {
        $fileName   = 'forecast.xml';
        $content    = $this->getLayout()->createBlock('ecomgems_salesforecast/adminhtml_forecast_grid')
            ->getXml();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @access protected
     * @return boolean
     * 
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('ecomgems_salesforecast/forecast');
    }


    public function updateAmountAction()
    {
        $fieldId = (int) $this->getRequest()->getParam('id');
        $amount = $this->getRequest()->getParam('forecast_amount');
        if ($fieldId) {
            $model = Mage::getModel('ecomgems_salesforecast/forecast')->load($fieldId);
            $model->setForecastAmount($amount);
            if($model->save()){
                echo 'ok';
            }
        }
    }
}
