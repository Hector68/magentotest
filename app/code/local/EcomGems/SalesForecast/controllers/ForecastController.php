<?php

class EcomGems_SalesForecast_ForecastController extends Mage_Core_Controller_Front_Action
{


    public function indexAction()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()):
            $this->_redirect('customer/account/login');
            return;
        endif;
        $session = Mage::getSingleton('core/session');

        if ($forecastNew = Mage::app()->getRequest()->getPost('forecast_new')) {
            foreach ($forecastNew as $year_month => $amount) {
                if (empty($amount) === false) {

                    try {
                        $saveData = [];
                        list($saveData['year'], $saveData['month']) = explode('_', $year_month);
                        $customer = Mage::getSingleton('customer/session')->getCustomer();
                        $saveData['forecast_amount'] = $amount;
                        $saveData['customer_id'] = $customer->getId();
                        $saveData['customer_name'] = $customer->getName();

                        $model = Mage::getModel('ecomgems_salesforecast/forecast');
                        $model->setData($saveData);
                        $model->save();
                    } catch (Exception $e) {
                        $session->addError($this->__('Unable to Save ') .' '. $year_month .' '. $e->getMessage());
                    }
                }
            }
        }


        $this->loadLayout();
        $this->renderLayout();
    }

}