<?php

/**
 * Sales Forecast resource model
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast

 */
class EcomGems_SalesForecast_Model_Resource_Forecast extends Mage_Core_Model_Resource_Db_Abstract
{

    /**
     * constructor
     *
     * @access public
     * 
     */
    public function _construct()
    {
        $this->_init('ecomgems_salesforecast/forecast', 'entity_id');
    }
}
