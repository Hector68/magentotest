<?php

/**
 * Sales Forecast collection resource model
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast

 */
class EcomGems_SalesForecast_Model_Resource_Forecast_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected $_joinedFields = array();

    /**
     * constructor
     *
     * @access public
     * @return void
     * 
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('ecomgems_salesforecast/forecast');
    }

    /**
     * get sales forecasts as array
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @param array $additional
     * @return array
     * 
     */
    protected function _toOptionArray($valueField='entity_id', $labelField='forecast_amount', $additional=array())
    {
        return parent::_toOptionArray($valueField, $labelField, $additional);
    }

    /**
     * get options hash
     *
     * @access protected
     * @param string $valueField
     * @param string $labelField
     * @return array
     * 
     */
    protected function _toOptionHash($valueField='entity_id', $labelField='forecast_amount')
    {
        return parent::_toOptionHash($valueField, $labelField);
    }

    /**
     * Get SQL for get record count.
     * Extra GROUP BY strip added.
     *
     * @access public
     * @return Varien_Db_Select
     * 
     */
    public function getSelectCountSql()
    {
        $countSelect = parent::getSelectCountSql();
        $countSelect->reset(Zend_Db_Select::GROUP);
        return $countSelect;
    }
}
