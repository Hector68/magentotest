<?php

/**
 * Sales Forecast model
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast

 */
class EcomGems_SalesForecast_Model_Forecast extends Mage_Core_Model_Abstract
{
    /**
     * Entity code.
     * Can be used as part of method name for entity processing
     */
    const ENTITY    = 'ecomgems_salesforecast_forecast';
    const CACHE_TAG = 'ecomgems_salesforecast_forecast';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'ecomgems_salesforecast_forecast';

    /**
     * Parameter name in event
     *
     * @var string
     */
    protected $_eventObject = 'forecast';

    /**
     * constructor
     *
     * @access public
     * @return void
     * 
     */
    public function _construct()
    {
        parent::_construct();
        $this->_init('ecomgems_salesforecast/forecast');
    }

    /**
     * before save sales forecast
     *
     * @access protected
     * @return EcomGems_SalesForecast_Model_Forecast
     * 
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        $now = Mage::getSingleton('core/date')->gmtDate();
        if ($this->isObjectNew()) {
            $this->setCreatedAt($now);
        }
        $this->setUpdatedAt($now);
        return $this;
    }

    /**
     * save sales forecast relation
     *
     * @access public
     * @return EcomGems_SalesForecast_Model_Forecast
     * 
     */
    protected function _afterSave()
    {
        return parent::_afterSave();
    }

    /**
     * get default values
     *
     * @access public
     * @return array
     * 
     */
    public function getDefaultValues()
    {
        $values = array();
        return $values;
    }
    
}
