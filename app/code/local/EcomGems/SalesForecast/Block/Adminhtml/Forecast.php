<?php

/**
 * Sales Forecast admin block
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast
 */
class EcomGems_SalesForecast_Block_Adminhtml_Forecast extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->_controller         = 'adminhtml_forecast';
        $this->_blockGroup         = 'ecomgems_salesforecast';
        parent::__construct();
        $this->_headerText         = Mage::helper('ecomgems_salesforecast')->__('Sales Forecast');
        $this->_updateButton('add', 'label', Mage::helper('ecomgems_salesforecast')->__('Add Sales Forecast'));

    }
}
