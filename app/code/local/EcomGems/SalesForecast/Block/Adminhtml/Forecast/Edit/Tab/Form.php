<?php

/**
 * Sales Forecast edit form tab
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast

 */
class EcomGems_SalesForecast_Block_Adminhtml_Forecast_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * prepare the form
     *
     * @access protected
     * @return EcomGems_SalesForecast_Block_Adminhtml_Forecast_Edit_Tab_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $form->setHtmlIdPrefix('forecast_');
        $form->setFieldNameSuffix('forecast');
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'forecast_form',
            array('legend' => Mage::helper('ecomgems_salesforecast')->__('Sales Forecast'))
        );

        $fieldset->addField(
            'customer_id',
            'text',
            array(
                'label' => Mage::helper('ecomgems_salesforecast')->__('Customer'),
                'name'  => 'customer_id',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'customer_name',
            'text',
            array(
                'label' => Mage::helper('ecomgems_salesforecast')->__('Customer name'),
                'name'  => 'customer_name',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'year',
            'text',
            array(
                'label' => Mage::helper('ecomgems_salesforecast')->__('Year'),
                'name'  => 'year',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'month',
            'text',
            array(
                'label' => Mage::helper('ecomgems_salesforecast')->__('Month'),
                'name'  => 'month',
                'required'  => true,
                'class' => 'required-entry',

           )
        );

        $fieldset->addField(
            'forecast_amount',
            'text',
            array(
                'label' => Mage::helper('ecomgems_salesforecast')->__('Forecast amount'),
                'name'  => 'forecast_amount',
                'required'  => true,
                'class' => 'required-entry',

           )
        );
        
        $formValues = Mage::registry('current_forecast')->getDefaultValues();
        if (!is_array($formValues)) {
            $formValues = array();
        }
        if (Mage::getSingleton('adminhtml/session')->getForecastData()) {
            $formValues = array_merge($formValues, Mage::getSingleton('adminhtml/session')->getForecastData());
            Mage::getSingleton('adminhtml/session')->setForecastData(null);
        } elseif (Mage::registry('current_forecast')) {
            $formValues = array_merge($formValues, Mage::registry('current_forecast')->getData());
        }
        $form->setValues($formValues);
        return parent::_prepareForm();
    }
}
