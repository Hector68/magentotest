<?php

/**
 * Sales Forecast admin edit form
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast
 */
class EcomGems_SalesForecast_Block_Adminhtml_Forecast_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * constructor
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'ecomgems_salesforecast';
        $this->_controller = 'adminhtml_forecast';
        $this->_updateButton(
            'save',
            'label',
            Mage::helper('ecomgems_salesforecast')->__('Save Sales Forecast')
        );
        $this->_updateButton(
            'delete',
            'label',
            Mage::helper('ecomgems_salesforecast')->__('Delete Sales Forecast')
        );
        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('ecomgems_salesforecast')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_formScripts[] = "
            function saveAndContinueEdit() {
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * get the edit form header
     *
     * @access public
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('current_forecast') && Mage::registry('current_forecast')->getId()) {
            return Mage::helper('ecomgems_salesforecast')->__(
                "Edit Sales Forecast '%s'",
                $this->escapeHtml(Mage::registry('current_forecast')->getForecastAmount())
            );
        } else {
            return Mage::helper('ecomgems_salesforecast')->__('Add Sales Forecast');
        }
    }
}
