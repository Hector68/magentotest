<?php

/**
 * Sales Forecast admin grid block
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast
 */
class EcomGems_SalesForecast_Block_Adminhtml_Forecast_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * constructor
     *
     * @access public
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('forecastGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    /**
     * prepare collection
     *
     * @access protected
     * @return EcomGems_SalesForecast_Block_Adminhtml_Forecast_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ecomgems_salesforecast/forecast')
            ->getCollection();
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * prepare grid collection
     *
     * @access protected
     * @return EcomGems_SalesForecast_Block_Adminhtml_Forecast_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id',
            array(
                'header' => Mage::helper('ecomgems_salesforecast')->__('Id'),
                'index'  => 'entity_id',
                'type'   => 'number'
            )
        );
        $this->addColumn(
            'forecast_amount',
            array(
                'header'    => Mage::helper('ecomgems_salesforecast')->__('Forecast amount'),
                'align'     => 'left',
                'renderer'         => 'ecomgems_salesforecast/adminhtml_forecast_grid_column_renderer_inline',
                'index'     => 'forecast_amount',
            )
        );

        $this->addColumn(
            'customer_id',
            array(
                'header' => Mage::helper('ecomgems_salesforecast')->__('Customer'),
                'index'  => 'customer_id',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'customer_name',
            array(
                'header' => Mage::helper('ecomgems_salesforecast')->__('Customer name'),
                'index'  => 'customer_name',
                'type'=> 'text',

            )
        );
        $this->addColumn(
            'year',
            array(
                'header' => Mage::helper('ecomgems_salesforecast')->__('Year'),
                'index'  => 'year',
                'type'=> 'number',

            )
        );
        $this->addColumn(
            'month',
            array(
                'header' => Mage::helper('ecomgems_salesforecast')->__('Month'),
                'index'  => 'month',
                'type'=> 'number',

            )
        );
        $this->addColumn(
            'created_at',
            array(
                'header' => Mage::helper('ecomgems_salesforecast')->__('Created at'),
                'index'  => 'created_at',
                'width'  => '120px',
                'type'   => 'datetime',
            )
        );
        $this->addColumn(
            'action',
            array(
                'header'  =>  Mage::helper('ecomgems_salesforecast')->__('Action'),
                'width'   => '100',
                'type'    => 'action',
                'getter'  => 'getId',
                'actions' => array(
                    array(
                        'caption' => Mage::helper('ecomgems_salesforecast')->__('Edit'),
                        'url'     => array('base'=> '*/*/edit'),
                        'field'   => 'id'
                    )
                ),
                'filter'    => false,
                'is_system' => true,
                'sortable'  => false,
            )
        );
        $this->addExportType('*/*/exportCsv', Mage::helper('ecomgems_salesforecast')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('ecomgems_salesforecast')->__('Excel'));
        $this->addExportType('*/*/exportXml', Mage::helper('ecomgems_salesforecast')->__('XML'));
        return parent::_prepareColumns();
    }

    /**
     * prepare mass action
     *
     * @access protected
     * @return EcomGems_SalesForecast_Block_Adminhtml_Forecast_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('forecast');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'=> Mage::helper('ecomgems_salesforecast')->__('Delete'),
                'url'  => $this->getUrl('*/*/massDelete'),
                'confirm'  => Mage::helper('ecomgems_salesforecast')->__('Are you sure?')
            )
        );
       
        return $this;
    }

    /**
     * get the row url
     *
     * @access public
     * @param EcomGems_SalesForecast_Model_Forecast
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    /**
     * get the grid url
     *
     * @access public
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

    /**
     * after collection load
     *
     * @access protected
     * @return EcomGems_SalesForecast_Block_Adminhtml_Forecast_Grid
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}
