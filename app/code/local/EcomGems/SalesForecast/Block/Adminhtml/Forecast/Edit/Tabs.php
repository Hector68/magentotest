<?php

/**
 * Sales Forecast admin edit tabs
 *
 * @category    EcomGems
 * @package     EcomGems_SalesForecast

 */
class EcomGems_SalesForecast_Block_Adminhtml_Forecast_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    /**
     * Initialize Tabs
     *
     * @access public
     * 
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('forecast_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ecomgems_salesforecast')->__('Sales Forecast'));
    }

    /**
     * before render html
     *
     * @access protected
     * @return EcomGems_SalesForecast_Block_Adminhtml_Forecast_Edit_Tabs
     * 
     */
    protected function _beforeToHtml()
    {
        $this->addTab(
            'form_forecast',
            array(
                'label'   => Mage::helper('ecomgems_salesforecast')->__('Sales Forecast'),
                'title'   => Mage::helper('ecomgems_salesforecast')->__('Sales Forecast'),
                'content' => $this->getLayout()->createBlock(
                    'ecomgems_salesforecast/adminhtml_forecast_edit_tab_form'
                )
                ->toHtml(),
            )
        );
        return parent::_beforeToHtml();
    }

    /**
     * Retrieve sales forecast entity
     *
     * @access public
     * @return EcomGems_SalesForecast_Model_Forecast
     * 
     */
    public function getForecast()
    {
        return Mage::registry('current_forecast');
    }
}
