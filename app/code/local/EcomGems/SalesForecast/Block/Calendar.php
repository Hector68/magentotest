<?php


class EcomGems_SalesForecast_Block_Calendar extends Mage_Core_Block_Template
{

    protected function getSelectYear()
    {
        $requestYear = Mage::app()->getRequest()->get('year');

        return $requestYear ? $requestYear : $this->getCurrentYear();
    }

    protected function getCurrentYear()
    {
        return (int)date('Y');
    }

    public function getNexButtonUrl()
    {

        $nextYear = $this->getSelectYear() + 1;
        $urlParams = [];
        if ($nextYear != $this->getCurrentYear()) {
            $urlParams['year'] = $nextYear;
        }
        return $this->getUrl('*/*',
            ['_use_rewrite' => true, '_forced_secure' => true] + $urlParams

        );
    }

    protected function getCustomerMinYear()
    {

        /**
         * Get the resource model
         */
        $resource = Mage::getSingleton('core/resource');
        /** @var $readConnection Varien_Db_Adapter_Pdo_Mysql */
        $readConnection = $resource->getConnection('core_read');
        $tableName = $resource->getTableName('ecomgems_salesforecast/forecast');

        $select = $readConnection->select()
            ->from($tableName, ['year'])
            ->where('customer_id = ?', Mage::getSingleton('customer/session')->getCustomerId())
            ->order('year', 'asc');

        $res = $readConnection->fetchOne($select);

        if (!$res || $res > $this->getCurrentYear()) {
            $res = $this->getCurrentYear();
        }

        return $res;
    }

    public function getPreviousButtonUrl()
    {

        $minYear = $this->getCustomerMinYear();

        if($minYear >= $this->getSelectYear()) {
            return false;
        }

        $previousYear = $this->getSelectYear() - 1;
        $urlParams = [];
        if ($previousYear != $this->getCurrentYear()) {
            $urlParams['year'] = $previousYear;
        }
        return $this->getUrl('*/*',
            ['_use_rewrite' => true, '_forced_secure' => true] + $urlParams

        );
    }

    protected function getUserForecast()
    {
        $result = [];
        $items = Mage::getModel('ecomgems_salesforecast/forecast')
            ->getCollection()
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomerId())
            ->addFieldToFilter('year', $this->getSelectYear())
            ->toArray(['year', 'month', 'forecast_amount']);

        if (isset($items['items'])) {
            foreach ($items['items'] as $item) {
                $key = implode('_', [$item['year'], $item['month']]);
                $result[$key] = $item;
            }
        }
        return $result;
    }


    public function getCalendarItems()
    {

        $userForecastData = $this->getUserForecast();


        $result = [];
        $newName = 'forecast_new';
        $currentYear = $this->getSelectYear();

        for ($m = 1; $m <= 12; $m++) {
            $key = implode('_', [$currentYear, $m]);
            $userValue = isset($userForecastData[$key]) ? $userForecastData[$key]['forecast_amount'] : null;


            $result[$m] = [
                'name' => $newName . '[' . $key . ']',
                'year' => $currentYear,
                'month' => $m,
                'value' => $userValue,
                'isEditable' => ($userValue === null)
            ];
        }

        return $result;

    }


}