<?php


class MageAssist_DiscountDisplay_Model_Observer
{


    /**
     * @param Varien_Event_Observer $event
     */
    public function addOldPrice(Varien_Event_Observer $event)
    {

        try {
            if ($quoteItem = $event->getData('quote_item')) {

                $_product = $quoteItem->getProduct();

                if ($_product->getPrice() !== $_product->getFinalPrice()) {
                    /**
                     * @var $quoteItem Mage_Sales_Model_Quote_Item
                     */

                    $option      = $quoteItem->getOptionByCode('info_buyRequest');
                    $optionValue  = $option ? unserialize($option->getValue()) : [];

                    $optionValue['original_price_base'] =  Mage::helper('tax')->getPrice($_product, $_product->getPrice(), true);
                    $optionValue['catalog_price_discount_pr'] = Mage::helper('tax')->getPrice($_product, $_product->getPrice(), true) / Mage::helper('tax')->getPrice($_product, $_product->getFinalPrice(), true);

                    $optionValue['original_price'] = Mage::helper('directory')->currencyConvert(
                        $optionValue['original_price_base'],
                        Mage::app()->getStore()->getBaseCurrencyCode(),
                        Mage::app()->getStore()->getCurrentCurrencyCode()
                    );
                    $optionValue['original_price_base'] = round($optionValue['original_price_base'], 2);
                    $optionValue['original_price']      = round($optionValue['original_price'], 2);


                    $option->setValue(serialize($optionValue));
                }

            }
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }

    }


    /**
     * @param Varien_Event_Observer $event
     */
    public function changeAddress(Varien_Event_Observer $event)
    {

        try {
        /***
         * @var $address Mage_Sales_Model_Quote_Address
         */
        $address = $event->getData('data_object');

        $quote = $address->getQuote();
        foreach ($quote->getAllItems() as $quoteItem) {


            if ($quoteItem->getBuyRequest()->getOriginalPrice()
                && $quoteItem->getBuyRequest()->getCatalogPriceDiscountPr()
            ) {
                $_product = $quoteItem->getProduct();
                $option = $quoteItem->getOptionByCode('info_buyRequest');
                $optionValue = $option ? unserialize($option->getValue()) : [];


                if ($quote->getData('taxes_for_items')) {
                    $optionValue['original_price_base'] =
                        Mage::helper('tax')->getPrice($_product, $_product->getPrice(), true);
                } else {
                    $optionValue['original_price_base'] = $_product->getPrice();
                }

                $optionValue['original_price'] = Mage::helper('directory')->currencyConvert(
                    $optionValue['original_price_base'],
                    Mage::app()->getStore()->getBaseCurrencyCode(),
                    Mage::app()->getStore()->getCurrentCurrencyCode()
                );

                $optionValue['original_price_base'] = round($optionValue['original_price_base'], 2);
                $optionValue['original_price']      = round($optionValue['original_price'], 2);


                if ($quoteItem->getBuyRequest()->getOriginalPrice() !== $optionValue['original_price']) {
                    $option->setValue(serialize($optionValue));
                    $option->save();
                }


            }


        }
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }



    }

}