<?php


class MageAssist_DiscountDisplay_Model_Quote_Discountcatalog extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $_code = 'discountcatalog';



    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {

        try {

            $amountCatalogDiscount = Mage::helper('ma_ddisplay')->getProductDiscountByAddress($address);

            if ($amountCatalogDiscount > 0.01) {


      
                $address->addTotal(
                    [
                        'code'  => $this->getCode(),
                        'title' => Mage::helper('ma_ddisplay')->__('Discount catalog'),
                        'value' =>  $amountCatalogDiscount
                    ]
                );
            }
        } catch (Exception $e) {
        }

        return $this;
    }
}