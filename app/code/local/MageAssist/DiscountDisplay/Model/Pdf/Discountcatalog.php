<?php


class MageAssist_DiscountDisplay_Model_Pdf_Discountcatalog extends Mage_Sales_Model_Order_Pdf_Total_Default
{
    /**
     * Get array of arrays with totals information for display in PDF
     * array(
     *  $index => array(
     *      'amount'   => $amount,
     *      'label'    => $label,
     *      'font_size'=> $font_size
     *  )
     * )
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;
        $amount = Mage::helper('ma_ddisplay')->getProductDiscountByOrder($this->getOrder());
        
        if ($amount > 0.01) {
            return array(array(
                             'amount'    => '-'.$this->getOrder()->formatPriceTxt($amount),
                             'label'     =>  Mage::helper('ma_ddisplay')->__('Discount catalog') . ':',
                             'font_size' => $fontSize
                         ));
        }

            return [];
    }
}