<?php

class MageAssist_DiscountDisplay_Helper_Data extends Mage_Core_Helper_Abstract
{
    

    public function getQuotePrice(Mage_Sales_Model_Quote_Item $item)
    {

        $_incl = Mage::helper('checkout')->getPriceInclTax($item);

        return $_incl - $item->getWeeeTaxDisposition();
    }

    public function getOrderPrice(Mage_Sales_Model_Order_Item $item)
    {
        return round(Mage::helper('checkout')->getPriceInclTax($item) - $item->getWeeeTaxDisposition(), 2);

    }

    public function getOrderPriceBase(Mage_Sales_Model_Order_Item $item)
    {
        return  round(Mage::helper('checkout')->getBasePriceInclTax($item) - $item->getBaseWeeeTaxDisposition(), 2);
    }

    public function getProductDiscountByAddress(Mage_Sales_Model_Quote_Address $address)
    {
        $amountCatalogDiscount = 0;

        try{

        $items = $address->getAllNonNominalItems();
        if (!count($items)) {
            return $amountCatalogDiscount; //this makes only address type shipping to come through
        }

        foreach ($items as $item) {
            if($originalPrice = $item->getBuyRequest()->getOriginalPrice())
            {
                $currentPrice = $this->getQuotePrice($item);


                if($currentPrice < $originalPrice) {
                    $amountCatalogDiscount += ($originalPrice - $currentPrice) * $item->getQty();
                }

            }
        }
        } catch (Exception $e) {
        }

        return $amountCatalogDiscount;
    }


    public function getProductDiscountByOrder(Mage_Sales_Model_Order $order)
    {
        $amountCatalogDiscount = 0;
        try{

            /**
             * @var $items Mage_Sales_Model_Order_Item[]
             */
           $items = $order->getAllItems();
            foreach ($items as $item) {
                if($originalPrice = $item->getBuyRequest()->getOriginalPrice())
                {
                    $currentPrice = $this->getOrderPrice($item);

                    if($currentPrice < $originalPrice) {
                        $amountCatalogDiscount += ($originalPrice - $currentPrice) * $item->getQtyOrdered();
                    }

                }
            }


        } catch (Exception $e){}

        return   $amountCatalogDiscount;
    }

    public function getProductDiscountByOrderBase(Mage_Sales_Model_Order $order)
    {
        $amountCatalogDiscount = 0;
        try{

            /**
             * @var $items Mage_Sales_Model_Order_Item[]
             */
            $items = $order->getAllItems();
            foreach ($items as $item) {
                if($originalPrice = $item->getBuyRequest()->getOriginalPriceBase())
                {
                    $currentPrice = $this->getOrderPriceBase($item);

                    if($currentPrice < $originalPrice) {
                        $amountCatalogDiscount += ($originalPrice - $currentPrice) * $item->getQtyOrdered();
                    }

                }
            }


        } catch (Exception $e){}

        return   $amountCatalogDiscount;
    }


    public function setDiscountByOrderItem($item)
    {

        /**
         * @var $item Mage_Sales_Model_Order_Item
         */
        $_item = $item instanceof Mage_Sales_Model_Order_Item ? $item : $item->getOrderItem();

        $result = [];

        if($_item->getBuyRequest()->getOriginalPrice() &&
           $_item->getBuyRequest()->getOriginalPrice() -  $this->getOrderPrice($_item)  >= 0.01) {

            $result = [
                'original_price_base' => $_item->getBuyRequest()->getOriginalPriceBase() ? $_item->getBuyRequest()->getOriginalPriceBase() :  $_item->getBuyRequest()->getOriginalPrice(),
                'original_price' => $_item->getBuyRequest()->getOriginalPrice()
            ];


            $discountBase = $item->getBaseDiscountAmount();
            $phpDiscountPriceCart = $_item->getBuyRequest()->getOriginalPriceBase() - ( Mage::helper('checkout')->getBasePriceInclTax($_item) - $item->getBaseWeeeTaxDisposition());

            $result['base_discount_amount'] = $phpDiscountPriceCart * $_item->getQtyOrdered() + $discountBase;


            $discount = $item->getDiscountAmount();
            $phpDiscountPriceCart = $_item->getBuyRequest()->getOriginalPrice() - ( Mage::helper('checkout')->getPriceInclTax($_item) - $item->getWeeeTaxDisposition());

            $result['discount_amount'] = $phpDiscountPriceCart * $_item->getQtyOrdered() + $discount;



        }

        return $result;


        }




    
}