<?php


class MageAssist_DiscountDisplay_Block_Discount extends Mage_Core_Block_Template
{

    protected $_template = 'mageassist/checkout/cart/item/discount.phtml';

    protected $_item;

    protected $_orderItem;

    protected $_currentPrice;

    protected $_oldPrice;

    protected $_qty = 1;


    /**
     * @param Mage_Sales_Model_Quote_Item_Abstract $item
     *
     * @return $this
     */
    public function setItem(Mage_Sales_Model_Quote_Item_Abstract $item)
    {
        $this->_item = $item;

        return $this;
    }

    /**
     * @param Mage_Sales_Model_Order_Item $orderItem
     *
     * @return $this
     */
    public function setOrderItem(Mage_Sales_Model_Order_Item $orderItem)
    {
        $this->_orderItem = $orderItem;

        return $this;

    }


    public function setQty($qty)
    {
        $this->_qty = $qty;
        return $this;
    }

    public function getQty()
    {
        return $this->_qty;
    }

    /**
     * Get quote item
     *
     * @return Mage_Sales_Model_Quote_Item|null
     */
    public function getItem()
    {
        return $this->_item;
    }


    public function getCurrencyCode()
    {
        $item = $this->getItem() ? $this->getItem() : $this->getOrderItem();

        $currencyCode =  Mage::app()->getStore()->getCurrentCurrencyCode();

        if ($item instanceof Mage_Sales_Model_Order_Item) {
            $currencyCode = $item->getOrder()->getOrderCurrencyCode();
        }

        return $currencyCode;
    }


    /**
     * @return Mage_Sales_Model_Order_Item|null
     */
    public function getOrderItem()
    {
        return $this->_orderItem;
    }


    public function getOffPercentages()
    {
        return  round((($this->getOldPrice() - $this->getCurrentPrice())*100) / $this->getOldPrice(), 0);
    }


    /**
     * @return float|null
     */
    public function getCurrentPrice()
    {
        if ($this->_currentPrice === null && ($this->getItem() || $this->getOrderItem())) {
            $item = $this->getItem() ? $this->getItem() : $this->getOrderItem();

            if ($item instanceof Mage_Sales_Model_Quote_Item_Abstract) {

                if (Mage::helper('weee')->typeOfDisplay($item, [0, 1, 4], 'sales')
                    && $item->getWeeeTaxAppliedAmount()
                ) {
                    $this->_currentPrice =
                        $item->getCalculationPrice() +
                        $item->getWeeeTaxAppliedAmount() +
                        $item->getWeeeTaxDisposition();
                } else {
                    $this->_currentPrice = $item->getCalculationPrice();
                }
            } elseif ($item instanceof Mage_Sales_Model_Order_Item) {
                if (Mage::helper('weee')->typeOfDisplay($item, [0, 1, 4], 'sales')
                    && $item->getWeeeTaxAppliedAmount()
                ) {
                    $this->_currentPrice =
                        $item->getPrice() +
                        $item->getWeeeTaxAppliedAmount() +
                        $item->getWeeeTaxDisposition();
                } else {
                    $this->_currentPrice = $item->getPrice();
                }
            }

        }

        return $this->_currentPrice ? $this->_currentPrice : null;
    }


    public function setCurrentPrice($currentPrice)
    {
        $this->_currentPrice = $currentPrice;
        return $this;
    }


    /**
     * get original product price
     *
     * @return float|null
     */
    public function getOldPrice()
    {
        if ($this->_oldPrice === null && ($this->getItem() || $this->getOrderItem())) {

            $item            = $this->getItem() ? $this->getItem() : $this->getOrderItem();
            

            $this->_oldPrice = $item->getBuyRequest()->getOriginalPrice()
                ?
                $item->getBuyRequest()->getOriginalPrice()
                :
                false;


        }

        return $this->_oldPrice ? $this->_oldPrice * $this->getQty() : null;
    }

    /**
     * @return bool
     */
    public function isDisplay()
    {
        return  $this->getOldPrice() &&
               $this->getCurrentPrice() &&
               $this->getOldPrice() > $this->getCurrentPrice() &&
               $this->getOffPercentages() > 1;
    }


}