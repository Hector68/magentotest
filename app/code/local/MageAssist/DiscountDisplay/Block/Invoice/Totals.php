<?php


class MageAssist_DiscountDisplay_Block_Invoice_Totals extends Mage_Sales_Block_Order_Invoice_Totals
{

    /**
     * Initialize order totals array
     *
     * @return Mage_Sales_Block_Order_Totals
     */
    protected function _initTotals()
    {
        parent::_initTotals();

        $amount = Mage::helper('ma_ddisplay')->getProductDiscountByOrder($this->getOrder());
        $amountBase = Mage::helper('ma_ddisplay')->getProductDiscountByOrderBase($this->getOrder());


        if ($amount > 0.01) {
            $this->addTotalBefore(
                new Varien_Object(
                    [
                        'code'       => 'discountcatalog',
                        'value'      => $amount,
                        'base_value' => -$amountBase,
                        'label'      => Mage::helper('ma_ddisplay')->__('Discount catalog'),
                    ], ['shipping', 'tax']
                )
            );
        }

        return $this;

    }

}