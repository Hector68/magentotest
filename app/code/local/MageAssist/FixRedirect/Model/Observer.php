<?php


class MageAssist_FixRedirect_Model_Observer extends Amasty_GeoipRedirect_Model_Observer
{

    public function redirectStore($observer) {
        parent::redirectStore($observer);
        $cookie = Mage::getSingleton('core/cookie');
        $cookie->set('ma_no_redirect', 1);
    }

    protected function applyLogic()
    {
        $result = parent::applyLogic();
        $cookie = Mage::getSingleton('core/cookie');
        if($cookie->get('ma_no_redirect') == 1) {
            $this->redirectAllowed = false;
        }
        
        return $result;
    }


}